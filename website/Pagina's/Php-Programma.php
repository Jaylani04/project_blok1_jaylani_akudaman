<?php
    
    $my_dice    = rand(1, 6);
    $other_dice = rand(1, 6);



if ($my_dice > $other_dice) {
    $result ="hoger";
}
elseif ($my_dice < $other_dice) {
   $result ="lager";
}
else  {
    $result ="even hoog";
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Project Blok 1</title>
    <link rel="stylesheet" href="../app.css">
</head>
<body>
    <header>
        <h2><a href="../index.html">Jaylani</a></h2>
        <nav>
            <ul>
                <li><a href="../index.html">Home</a></li>
                <li><a href="Mijzelf.html">Mijzelf</a></li>
                <li><a href="School.html">School</a></li>
                <li><a href="PHP&JS.html">PHP & JS</a></li>
                <li><a href="Rooster.html">Rooster</a></li>
                <li><a href="JS-Programma.html">JS-Programma</a></li>
                <li><a href="Php-Programma.php">Php-Programma</a></li>
                <li><a href="NL & ENG.html">NL & ENG</a></li>
            </ul>
        </nav>
    </header>

    <main> 
   
    <h1>Dobbelsteen</h1>

    <p>Het Php-programma dat ik gemaakt heb zijn dobbelstenen gooien. <br>
        (Refresh de pagina om opnieuw te gooien)
    </p>


    Ik heb <?=$my_dice?> gegooid. De ander heeft <?=$other_dice?> <br>
     
    Dus ik heb <?=$result?> gegooid.
    <p>
        Dit is de code (alleen de backend) die ik heb gebruikt voor de dobbelstenen: <br>
        Rand(1, 6) betekend dat je een willekeurig getal krijgt tussen de 1 en de 6.
        De If-statement word gebruikt voor dit zinnetje "dus ik heb ... gegooit".
        <img class="code_programma_php" src="../images/Code_programma_php.JPG" alt="">

    </p>

     
 

    </main>

    <footer>

        <a href="contact.html" class="button">Contact</a>
    
    </footer>
</body>
</html>