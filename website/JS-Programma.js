function JayTime() {


    let currentTime = new Date();                   // betekend datum van nu
    let hour = currentTime.getHours();              
    let min = currentTime.getMinutes();             // deze 3 variabelen zijn de secondes, minuten en uren
    let sec = currentTime.getSeconds();
    let period = "am";                              // deze variabele geeft de ochtend aan

    if ( hour >= 12 ) {
        period = "pm"                               // het gelijk of over aan 12 is word het pm (middag)
    }
    if ( hour > 12) {
        hour = hour - 12                            // als het 13 uur gaat er 12 vanaf zadat het 1pm aangeeft inplaats 13pm
    }
    if ( min < 10) {                                // als het kleiner dan 10 is dan zetten we er een 0 vol zodat er 12:01 inplaats van 12:1     
        min = "0" + min
    }
    if ( sec <10) {                                 // hier doen we precies het zelfde maar dan voor secondes
        sec = "0" + sec
    }

    document.getElementById("current_time").innerHTML = "de tijd is: " + hour + ":" + min + ":" + sec +  " " + period   // hier mee toon je de klok op je pagina
}

setInterval (JayTime,1000);                         // zorgt ervoor dat programma om de 1 seconde refresht zodat hij live blijft tellen